<?php

namespace App\Http\Controllers;

use App\Http\Requests\PageFormRequest;
use App\Page;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use App\Components\FlashMessages;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Session;

class WebserviceController extends Controller
{
    public function __construct()
    {
        //$this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    //---user login/register  type POST-//
    public function register(Request $request)
    {
    	header('Access-Control-Allow-Origin: *');

        // ALLOW OPTIONS METHOD
        $headers = [
            'Access-Control-Allow-Methods' => 'POST, GET, OPTIONS, PUT, DELETE',
            'Access-Control-Allow-Headers' => 'Content-Type, X-Auth-Token, Origin, Authorization'
        ];
      if(isset($_POST) && !empty($_POST)){
        $type     = (@$_POST['type']) ?: 'Normal';
        $name     = (@$_POST['name']) ?: ''; 
        $email    = (@$_POST['email']) ?: '';
        $phone    = (@$_POST['phone']) ?: '';
        $social_image    = (@$_POST['social_image']) ?: '';
        $instagram = "";
        $nameslug = "";
        $register = DB::table('users')
              ->where('email', $email)
              ->orWhere('phone', $phone)
              ->first();
        $nameslug = str_slug($name, '');
        $slug = DB::table('users')->where('slug', $nameslug)->first();
        if(empty($slug)){
          $nameslug = $nameslug;
        }else{
          $nameslug = $nameslug.rand('10','999');
          $slug1 = DB::table('users')->where('slug', $nameslug)->first();
          if(empty($slug1)){
            $nameslug = $nameslug;
          }
        }
        if(empty($register)){
          $update=DB::table('users')
            ->insert(array(
            'name'  => $name,
            'slug'  => $nameslug,
            'email' =>  $email,
            'phone' =>  $phone,
            'social_type' =>  $type,
            'social_image' =>  $social_image,
            'instagram'   =>  $instagram
          ));
          return response()->json([
              'status' => '200',
              'message' => 'Register Successfully',
              'data' => $register
          ]);
        }else{
          $type     = (@$_POST['type']) ?: $register->social_type;
          $name     = (@$_POST['name']) ?: $register->name;
          $email    = (@$_POST['email']) ?: $register->email;
          $phone    = (@$_POST['phone']) ?: $register->phone;
          $social_image    = (@$_POST['social_image']) ?: $register->social_image;
          if($type == "instagram"){
            $instagram =  $email;
          }
          $update=DB::table('users')
            ->where('id', $register->id)
            ->update(array(
            'name'  => $name,
            'email' =>  $email,
            'phone' =>  $phone,
            'social_type' =>  $type,
            'social_image' =>  $social_image,
            'instagram'   =>  $instagram
          ));
          $login = DB::table('users')
              ->where('email', $email)
              ->orWhere('phone', $phone)
              ->first();
          return response()->json([
            'status' => '200',
            'message' => 'Login Successfully!',
            'data' => $login
          ]);
        }
      }else{
        return response()->json([
            'status' => '201',
            'message' => 'Same error found !'
        ]);
      }
    }
    //---user profile- type GET//
    public function profile($id=null){
      header('Access-Control-Allow-Origin: *');
        // ALLOW OPTIONS METHOD
        $headers = [
            'Access-Control-Allow-Methods' => 'POST, GET, OPTIONS, PUT, DELETE',
            'Access-Control-Allow-Headers' => 'Content-Type, X-Auth-Token, Origin, Authorization'
        ];
      if(isset($id) && !empty($id)){
        $user['profile'] = DB::table('users')
              ->where('id', $id)
              ->first();
        $like_video = DB::table('video')
              ->leftJoin('like_video', 'like_video.video_id', '=', 'video.id')
              ->select('video.id',DB::raw("count(like_video.video_id) AS total_hearts"))
              ->where('video.user_id',$id)
              ->first();
        $user['total_hearts'] = $like_video->total_hearts;
        $user['follow'] = DB::table('follow')
            ->where('receiver',$id)
            ->where('status','1')
            ->count();
        $user['fans'] = DB::table('follow')
            ->where('receiver',$id)
            ->where('status','1')
            ->count();
        $user['video'] = DB::table('video')
              ->leftJoin('like_video', 'like_video.video_id', '=', 'video.id')
              ->select('video.id','video.user_id','video.video',
              DB::raw("count(like_video.video_id) AS total_like"))
              ->where('video.user_id',$id)
              ->orderBy('id','DESC')
              ->groupBy('video.id')
              ->get(); 
        $user['like'] = DB::table('like_video')
              ->leftJoin('video', 'video.id', '=', 'like_video.video_id')
              ->select('video.id','video.user_id','video.video',
              DB::raw("count(like_video.video_id) AS total_like"))
              ->where('like_video.user_id',$id)
              ->orderBy('id','DESC')
              ->groupBy('like_video.id')
              ->get();
        //echo "<pre>"; print_r($user['like']); die;
        if(!empty($user)){  
          return response()->json([
              'status' => '200',
              'message' => 'User profile',
              'data' => $user
          ]);
        }else{
           return response()->json([
            'status' => '201',
            'message' => 'not exit!'
          ]);
        }
      }else{
        return response()->json([
            'status' => '201',
            'message' => 'Same error found !'
        ]);
      }
    }
    //---user update-profile -type POST//
    public function update_profile(Request $request)
    {
      header('Access-Control-Allow-Origin: *');

        // ALLOW OPTIONS METHOD
        $headers = [
            'Access-Control-Allow-Methods' => 'POST, GET, OPTIONS, PUT, DELETE',
            'Access-Control-Allow-Headers' => 'Content-Type, X-Auth-Token, Origin, Authorization'
        ];
      if(isset($_POST) && !empty($_POST)){
        $id     = (@$_POST['id']) ?: '';
        $name1     = (@$_POST['name']) ?: '';
        $slug     = (@$_POST['slug']) ?: ''; 
        $bio    = (@$_POST['bio']) ?: '';
        $instagram    = (@$_POST['instagram']) ?: '';
        $youtube    = (@$_POST['youtube']) ?: '';
        $oldpicture    = (@$_POST['oldpicture']) ?: '';
          if(!empty($_FILES['picture']['name'])){
            $image = $_FILES['picture']['name'];
            $temp_anme = $_FILES["picture"]["tmp_name"];
            $name = rand(10,99999).$image;
            $uploadfile = $_SERVER['DOCUMENT_ROOT'] . '/jatt_juliet/public/users/';
            $imagePath = $uploadfile. $name;
            move_uploaded_file($temp_anme,$imagePath);
            @unlink($uploadfile.$oldpicture);
            $hm_image = $name;
          }else{
            $hm_image = $oldpicture;
          }
          $update=DB::table('users')
            ->where('id', $id)
            ->update(array(
            'name' =>  $name1,
            'slug' =>  $slug,
            'picture' => $hm_image,
            'bio' =>  $bio,
            'instagram' =>  $instagram,
            'youtube'   =>  $youtube
          ));
          return response()->json([
            'status' => '200',
            'message' => 'Update Successfully!',
            'data' => $_POST
          ]);
      }else{
        return response()->json([
            'status' => '201',
            'message' => 'Same error found !'
        ]);
      }
    }
    //---user chack-slug -type POST//
    public function chack_slug(Request $request)
    {
      header('Access-Control-Allow-Origin: *');

        // ALLOW OPTIONS METHOD
        $headers = [
            'Access-Control-Allow-Methods' => 'POST, GET, OPTIONS, PUT, DELETE',
            'Access-Control-Allow-Headers' => 'Content-Type, X-Auth-Token, Origin, Authorization'
        ];
      if(isset($_POST) && !empty($_POST)){
        $id     = (@$_POST['id']) ?: '';
        $slug     = (@$_POST['slug']) ?: ''; 
        $user = DB::table('users')
              ->where('slug', $slug)
              ->where('id', '!=',  $id)
              ->first();
        if(!empty($user)){
          return response()->json([
              'status' => '201',
              'message' => 'Username exit',
          ]);
        }else{
           return response()->json([
            'status' => '200',
            'message' => 'Username not exit!'
          ]);
        }
      }else{
        return response()->json([
            'status' => '201',
            'message' => 'Same error found !'
        ]);
      }
    }
    //---user all video- type GET//
    public function video(){
      header('Access-Control-Allow-Origin: *');
        // ALLOW OPTIONS METHOD
        $headers = [
            'Access-Control-Allow-Methods' => 'POST, GET, OPTIONS, PUT, DELETE',
            'Access-Control-Allow-Headers' => 'Content-Type, X-Auth-Token, Origin, Authorization'
        ];
        $video = DB::table('users')
              ->leftJoin('video', 'video.user_id', '=', 'users.id')
              ->leftJoin('like_video', 'like_video.video_id', '=', 'video.id')
              ->select('video.id','video.video','users.picture','users.social_image',
              DB::raw("count(like_video.video_id) AS total_like"))
              ->where('users.status','1')
              ->orderBy('id','DESC')
              ->groupBy('video.id')
              ->get(); 
        //echo "<pre>"; print_r($video); die;
        if(!empty($video)){  
          return response()->json([
              'status' => '200',
              'message' => 'Video list',
              'data' => $video
          ]);
        }else{
           return response()->json([
            'status' => '201',
            'message' => 'not exit!'
          ]);
        }
    }
}
