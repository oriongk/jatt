<?php

namespace App\Http\Controllers;

use App\Http\Requests\PageFormRequest;
use App\Page;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Redirect;
use App\Components\FlashMessages;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Session;
class PageController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
      // $user = Auth::user();
      // $status = $user->status;
      // if($status == "0"){
      //   return view('page/blocked');
      // }
      $users = DB::table('users')
            ->count();
      $admin = DB::table('admin')
            ->where('role', '2')
            ->count(); 
      $video = DB::table('video')
            ->count(); 
      $sound = DB::table('sound')
            ->count(); 
       $active = "Dashboard";
       $active_menu = "Dashboard";
       return view('page/index', compact('active','active_menu','admin','users','video','sound'));
    }

    // This function use for manage-users-users START --
    public function users()
    {
      //echo "<pre>"; print_r($_GET); die;
    	$user = Auth::user();
    	$permission = $user->permission;
		  $permission = explode(",", $permission);
     	if($user->role == "1" || in_array("1", $permission)){ 
        $paginationlimit = '1';
        $pages = DB::table('users')
                ->orderBy('id','DESC');
        if(isset($_GET['limit']) && !empty($_GET['limit'])){
          $paginationlimit = $_GET['limit'];
        }
        if(isset($_GET['name']) && !empty($_GET['name'])){
          $name = $_GET['name'];
          $pages = $pages->orwhere('name', 'like', "%$name%")
          ->orwhere('name', 'like', "%$name%")
          ->orwhere('slug', 'like', "%$name%")
          ->orwhere('phone', 'like', "%$name%");
        } 
        if(isset($_GET['daterange']) && !empty($_GET['daterange'])){
          $daterange = $_GET['daterange'];
          //echo $daterange; die;
          $daterange = explode(" - ", $daterange);
          $startdate = $daterange[0];
          $enddate = $daterange[1];
          $today = date("Y-m-d");
          if($startdate != $today){
            // echo $startdate."==".$enddate; die;
            $pages = $pages->WhereBetween('created_at',[$startdate, $enddate]);
          }
        }
        if(isset($_GET['status']) && !empty($_GET['status'])){
          $status = $_GET['status'];
          if($status != "Select status"){
            if($status == 2){
              $status = 0;
            }else{
              $status = 1;
            }
            // echo $startdate."==".$enddate; die;
            $pages = $pages->Where('status',$status);
          }
        }
        $pages = $pages->paginate($paginationlimit);
	      //echo "<pr>"; print_r($pages); die;
	      $active = "Users";
	      $active_menu = "ManageUsers"; 
	      return view('page/users',compact('pages','active','active_menu'));
  		}else{
       		return Redirect::to('/');
  		}
    }
    public function view_users($id)
    {
    	$user = Auth::user();
    	$permission = $user->permission;
		  $permission = explode(",", $permission);
     	if($user->role == "1" || in_array("1", $permission)){  
	      $pages=DB::table('users')
	            ->where('id', $id)->get();
	      // echo "<pr>"; print_r($pages);
	      $active = "Users";
	      $active_menu = "ManageUsers"; 
	      return view('page/view_users',compact('pages','active','active_menu'));
	    }else{
       		return Redirect::to('/');
  		}
    }
    public function remove_users(Request $request)
    {

      $teams = $_POST['clients'];
      $teams=DB::table('users')->whereIn('id', $teams)->delete();
      //echo "<pre>"; print_r($_POST); die;
      $request->session()->flash('success', 'Your User has been deleted');
      //return Redirect::to('/manage-users');
      return redirect()->back();
    }
    public function update_users()
    {
      //echo "<pre>"; print_r($_GET); die;
      $id    = $_POST['id'];
      $status = $_POST['type'];
      
       if($status == 1){
          $status1 = "0";
        }else{
          $status1 = "1";
        }
        $update=DB::table('users')
            ->where('id', $id)
            ->update(array(
            'status' => $status1
        ));
        if($status == 1){
          return "0";
        }else{
          return "1";
        }
      
    }
    // This function use for manage-users-users END --
    // This function use for manage-category START --
    public function manage_category()
    {
    	$user = Auth::user();
    	$permission = $user->permission;
		  $permission = explode(",", $permission);
     	if($user->role == "1" || in_array("3", $permission)){  
        $paginationlimit = '2';
	        $pages = DB::table('category')
	            ->orderBy('category_id','DESC');
          if(isset($_GET['limit']) && !empty($_GET['limit'])){
            $paginationlimit = $_GET['limit'];
          }
          if(isset($_GET['name']) && !empty($_GET['name'])){
            $name = $_GET['name'];
            $pages = $pages->where('category_title', 'like', "%$name%");
          }
          if(isset($_GET['daterange']) && !empty($_GET['daterange'])){
            $daterange = $_GET['daterange'];
            //echo $daterange; die;
            $daterange = explode(" - ", $daterange);
            $startdate = $daterange[0];
            $enddate = $daterange[1];
            $today = date("Y-m-d");
            if($startdate != $today){
              // echo $startdate."==".$enddate; die;
              $pages = $pages->WhereBetween('category_added_date',[$startdate, $enddate]);
            }
          }
          $pages = $pages->paginate($paginationlimit);
	        $active = "Categories";
	        $active_menu = "SoundC";
	        return view('page/manage_category',compact('pages','active','active_menu'));
	    }else{
       		return Redirect::to('/');
  		}
    }
    public function add_category()
    {
    	$user = Auth::user();
    	$permission = $user->permission;
		  $permission = explode(",", $permission);
     	if($user->role == "1" || in_array("3", $permission)){  
	        $active = "Categories";
	        $active_menu = "SoundC";
	        return view('page/add_category',compact('active','active_menu'));
	    }else{
       		return Redirect::to('/');
  		}
    }
    public function save_category(Request $request)
    {
      //echo "<pre>"; print_r($_POST); die;
      $category_title  = $_POST['category_title'];
      $today = date("Y-m-d h:i:sa"); 
      if(!empty($_FILES['category_image']['name'])){
          $image = $_FILES['category_image']['name'];
          $temp_anme = $_FILES["category_image"]["tmp_name"];
          $name = rand(10,99999).$image;
          $uploadfile = $_SERVER['DOCUMENT_ROOT'] . '/jatt_juliet/public/category/';
          //echo $uploadfile; die;
          //$destinationPath = url('/').'/home/';
          $imagePath = $uploadfile. $name;
          //echo $temp_anme."<br>".$imagePath; die;
          move_uploaded_file($temp_anme,$imagePath);
          $hm_image = $name;
      }else{
          $hm_image = "";
      }
      $update=DB::table('category')
          ->insert(array(
          'category_title'      => $category_title,
          'category_image'      => $hm_image,
          'category_added_date' => $today
      ));
      $request->session()->flash('success', 'Your category has been added');
      return Redirect::to('/manage-category');
    }
    public function edit_category($id)
    {
    	$user = Auth::user();
    	$permission = $user->permission;
		  $permission = explode(",", $permission);
     	if($user->role == "1" || in_array("3", $permission)){ 
			$pages=DB::table('category')
			->where('category_id', $id)->get();
			// echo "<pr>"; print_r($pages);
			$active = "Categories";
			$active_menu = "SoundC";
			return view('page/edit_category',compact('pages','active','active_menu'));
		}else{
       		return Redirect::to('/');
  		}
    }
    public function update_category(Request $request)
    {
      //echo "<pre>"; print_r($_POST); die;
      $category_id  = $_POST['category_id'];
      $category_title  = $_POST['category_title'];
      $old_image  = $_POST['old_image'];
      $today = date("Y-m-d h:i:sa"); 
      if(!empty($_FILES['category_image']['name'])){
          $image = $_FILES['category_image']['name'];
          $temp_anme = $_FILES["category_image"]["tmp_name"];
          $name = rand(10,99999).$image;
          $uploadfile = $_SERVER['DOCUMENT_ROOT'] . '/jatt_juliet/public/category/';
          //echo $uploadfile; die;
          //$destinationPath = url('/').'/home/';
          $imagePath = $uploadfile. $name;
          //echo $temp_anme."<br>".$imagePath; die;
          move_uploaded_file($temp_anme,$imagePath);
          @unlink($uploadfile.$old_image);
          $hm_image = $name;
      }else{
          $hm_image = $old_image;
      }
      $update=DB::table('category')
          ->where('category_id', $category_id)
          ->update(array(
          'category_title'      => $category_title,
          'category_image'      => $hm_image,
          'category_added_date' => $today
      ));
      $request->session()->flash('success', 'Your category has been updated');
      return Redirect::to('/manage-category');
    }
    public function remove_category(Request $request)
    {
      $teams = $_POST['clients'];
      $teams=DB::table('category')->whereIn('category_id', $teams)->delete();
      //echo "<pre>"; print_r($_POST); die;
      $request->session()->flash('success', 'Your category has been deleted');
      //return Redirect::to('/manage-category');
      return redirect()->back();
    }
    // This function use for manage-category END START --

    // This function use for manage-video START --
    public function manage_video()
    {
    	$user = Auth::user();
    	$permission = $user->permission;
		  $permission = explode(",", $permission);
     	if($user->role == "1" || in_array("2", $permission)){ 
        $paginationlimit = '2';
        $pages = DB::table('video')
                ->leftJoin('users', 'video.user_id', '=', 'users.id')
                ->select('video.*', 'users.name','users.slug')
                ->orderBy('id','DESC')
                ->groupBy('user_id');
        if(isset($_GET['limit']) && !empty($_GET['limit'])){
          $paginationlimit = $_GET['limit'];
        }
        if(isset($_GET['name']) && !empty($_GET['name'])){
          $name = $_GET['name'];
          $pages = $pages->orwhere('name', 'like', "%$name%")
          ->orwhere('users.name', 'like', "%$name%")
          ->orwhere('users.slug', 'like', "%$name%");
        } 
        if(isset($_GET['daterange']) && !empty($_GET['daterange'])){
          $daterange = $_GET['daterange'];
          //echo $daterange; die;
          $daterange = explode(" - ", $daterange);
          $startdate = $daterange[0];
          $enddate = $daterange[1];
          $today = date("Y-m-d");
          if($startdate != $today){
            // echo $startdate."==".$enddate; die;
            $pages = $pages->WhereBetween('video.created_at',[$startdate, $enddate]);
          }
        }
        $pages = $pages->paginate($paginationlimit);
        //echo "<pre>"; print_r($pages); die;
  			$active = "Video";
  			$active_menu = "AdminVideo";
  			return view('page/manage_video',compact('pages','active','active_menu'));
		  }else{
       		return Redirect::to('/');
  		}
    }
    public function view_video($id)
    {
    	$user = Auth::user();
    	$permission = $user->permission;
		  $permission = explode(",", $permission);
       	if($user->role == "1" || in_array("2", $permission)){
          $paginationlimit = '1';
          $pages = DB::table('video')
                  ->leftJoin('users', 'video.user_id', '=', 'users.id')
                  ->select('video.*', 'users.name','users.picture','users.email')
                  ->where('user_id',$id)
                  ->orderBy('id','DESC');
          if(isset($_GET['limit']) && !empty($_GET['limit'])){
            $paginationlimit = $_GET['limit'];
          } 
          if(isset($_GET['daterange']) && !empty($_GET['daterange'])){
            $daterange = $_GET['daterange'];
            //echo $daterange; die;
            $daterange = explode(" - ", $daterange);
            $startdate = $daterange[0];
            $enddate = $daterange[1];
            $today = date("Y-m-d");
            if($startdate != $today){
              // echo $startdate."==".$enddate; die;
              $pages = $pages->WhereBetween('video.created_at',[$startdate, $enddate]);
            }
          }
          $pages = $pages->paginate($paginationlimit);
    			// $pages = DB::table('video')
    			// ->leftJoin('users', 'video.user_id', '=', 'users.id')
    			// ->select('video.*', 'users.name','users.picture','users.email')
    			// ->where('user_id',$id)
    			// ->orderBy('id','DESC')
    			// ->get();
          //echo "<pre>"; print_r($pages); die;
    			$active = "Video";
    			$active_menu = "AdminVideo";
    			return view('page/view_video',compact('pages','active','active_menu'));
    		}else{
           		return Redirect::to('/');
      	}
    }
    public function delete_video($id)
    {
    	$user = Auth::user();
    	$permission = $user->permission;
		  $permission = explode(",", $permission);
     	if($user->role == "1" || in_array("2", $permission)){ 
			$teams=DB::table('video')->where('id', $id)->delete();
			//$request->session()->flash('success', 'Your video has been deleted');
			return redirect()->back();

		}else{
       		return Redirect::to('/');
  		}
    }
    public function remove_video(Request $request)
    {
      $teams = $_POST['teams'];
      $teams=DB::table('video')->whereIn('user_id', $teams)->delete();
      //echo "<pre>"; print_r($_POST); die;
      $request->session()->flash('success', 'Your video has been deleted');
      //return Redirect::to('/users-video');
      return redirect()->back();
    }
    // This function use for manage-video  END --
    
    // This function use for manage-sound START --
    public function manage_sound()
    {
    	$user = Auth::user();
    	$permission = $user->permission;
		  $permission = explode(",", $permission);
     	if($user->role == "1" || in_array("3", $permission)){
        $paginationlimit = '2';
  			$pages = DB::table('sound')
    			->leftJoin('category', 'category.category_id', '=', 'sound.category_id')
    			->select('sound.*', 'category.category_title')
    			->orderBy('id','DESC');
        if(isset($_GET['limit']) && !empty($_GET['limit'])){
          $paginationlimit = $_GET['limit'];
        }
        if(isset($_GET['name']) && !empty($_GET['name'])){
          $name = $_GET['name'];
          $pages = $pages->orwhere('sound.title', 'like', "%$name%")
          ->orwhere('category.category_title', 'like', "%$name%");
        } 
        if(isset($_GET['daterange']) && !empty($_GET['daterange'])){
          $daterange = $_GET['daterange'];
          //echo $daterange; die;
          $daterange = explode(" - ", $daterange);
          $startdate = $daterange[0];
          $enddate = $daterange[1];
          $today = date("Y-m-d");
          if($startdate != $today){
            // echo $startdate."==".$enddate; die;
            $pages = $pages->WhereBetween('sound.category_added_date',[$startdate, $enddate]);
          }
        }
        $pages = $pages->paginate($paginationlimit);
  			$active = "Sound";
  			$active_menu = "SoundC";
  			return view('page/manage_sound',compact('pages','active','active_menu'));
		  }else{
       		return Redirect::to('/');
  		}
    }
    public function add_sound()
    {
    	$user = Auth::user();
    	$permission = $user->permission;
		  $permission = explode(",", $permission);
     	if($user->role == "1" || in_array("3", $permission)){
			$pages=DB::table('category')
			->where('category_home_status', '1')->get();
			$active = "Sound";
			$active_menu = "SoundC";
			return view('page/add_sound',compact('active','active_menu','pages'));
		  }else{
       		return Redirect::to('/');
  		}
    }
    public function save_sound(Request $request)
    {
      //echo "<pre>"; print_r($_POST); print_r($_FILES); die;
      $category_id  = $_POST['category_id'];
      $title  = $_POST['title'];
      $today = date("Y-m-d h:i:sa"); 
      if(!empty($_FILES['sound']['name'])){
          $image = $_FILES['sound']['name'];
          $temp_anme = $_FILES["sound"]["tmp_name"];
          $name = rand(10,99999).$image;
          $uploadfile = $_SERVER['DOCUMENT_ROOT'] . '/jatt_juliet/public/category/';
          //echo $uploadfile; die;
          //$destinationPath = url('/').'/home/';
          $imagePath = $uploadfile. $name;
          //echo $temp_anme."<br>".$imagePath; die;
          move_uploaded_file($temp_anme,$imagePath);
          $hm_image = $name;
      }else{
          $hm_image = "";
      }
      if(!empty($_FILES['image']['name'])){
          $image1 = $_FILES['image']['name'];
          $temp_anme1 = $_FILES["image"]["tmp_name"];
          $name1 = rand(10,99999).$image1;
          $uploadfile1 = $_SERVER['DOCUMENT_ROOT'] . '/jatt_juliet/public/category/';
          //echo $uploadfile; die;
          //$destinationPath = url('/').'/home/';
          $imagePath1 = $uploadfile1. $name1;
          //echo $temp_anme."<br>".$imagePath; die;
          move_uploaded_file($temp_anme1,$imagePath1);
          $hm_image1 = $name1;
      }else{
          $hm_image1 = "";
      }
      $update=DB::table('sound')
          ->insert(array(
          'category_id'      => $category_id,
          'title'      => $title,
          'sound' => $hm_image,
          'image' => $hm_image1
      ));
      $request->session()->flash('success', 'Your sound has been added');
      return Redirect::to('/manage-sound');
    }
    public function edit_sound($id)
    {
    	$user = Auth::user();
    	$permission = $user->permission;
		  $permission = explode(",", $permission);
     	if($user->role == "1" || in_array("3", $permission)){
			$sound=DB::table('sound')
			    ->where('id', $id)->get();
			$pages=DB::table('category')
			    ->where('category_home_status', '1')->get();
			$active = "Sound";
			$active_menu = "SoundC";
			return view('page/edit_sound',compact('sound','pages','active','active_menu'));
      	}else{
       		return Redirect::to('/');
  		}
    }
    public function update_sound(Request $request)
    {
      //echo "<pre>"; print_r($_FILES); die;
      $id  = $_POST['id'];
      $category_id  = $_POST['category_id'];
      $title  = $_POST['title'];
      $old_image  = $_POST['old_image'];
      $old_audio  = $_POST['old_audio'];
      $today = date("Y-m-d h:i:sa"); 
      if(!empty($_FILES['sound']['name'])){
          $image = $_FILES['sound']['name'];
          $size = $_FILES['sound']['size'];
          if($size < 1000000){
            $temp_anme = $_FILES["sound"]["tmp_name"];
            $name = rand(10,99999).$image;
            $uploadfile = $_SERVER['DOCUMENT_ROOT'] . '/jatt_juliet/public/category/';
            //echo $uploadfile; die;
            //$destinationPath = url('/').'/home/';
            $imagePath = $uploadfile. $name;
            //echo $temp_anme."<br>".$imagePath; die;
            move_uploaded_file($temp_anme,$imagePath);
            @unlink($uploadfile.$old_audio);
            $hm_image = $name;
          }else{
            $request->session()->flash('error', 'Your sound too big');
      return Redirect::to('/manage-sound');
          }
      }else{
          $hm_image = "";
      }
      if(!empty($_FILES['image']['name'])){
          $image1 = $_FILES['image']['name'];
          $temp_anme1 = $_FILES["image"]["tmp_name"];
          $name1 = rand(10,99999).$image1;
          $uploadfile1 = $_SERVER['DOCUMENT_ROOT'] . '/jatt_juliet/public/category/';
          //echo $uploadfile; die;
          //$destinationPath = url('/').'/home/';
          $imagePath1 = $uploadfile1. $name1;
          //echo $temp_anme."<br>".$imagePath; die;
          move_uploaded_file($temp_anme1,$imagePath1);
          @unlink($uploadfile1.$old_image);
          $hm_image1 = $name1;
      }else{
          $hm_image1 = "";
      }
      $update=DB::table('sound')
          ->where('id', $id)
          ->update(array(
          'category_id'      => $category_id,
          'title'      => $title,
          'sound' => $hm_image,
          'image' => $hm_image1
      ));
      $request->session()->flash('success', 'Your sound has been updated');
      return Redirect::to('/manage-sound');
    }
    public function remove_sound(Request $request)
    {
      $teams = $_POST['clients'];
      $teams=DB::table('sound')->whereIn('id', $teams)->delete();
      //echo "<pre>"; print_r($_POST); die;
      $request->session()->flash('success', 'Your sound has been deleted');
      //return Redirect::to('/manage-sound');
      return redirect()->back();
    }
    // This function use for manage-sound END START --

    // This function use for manage-chat START --
    public function chat_support()
    {
    	$user = Auth::user();
    	$permission = $user->permission;
		  $permission = explode(",", $permission);
     	if($user->role == "1" || in_array("4", $permission)){
        $paginationlimit = '1';
        $pages = DB::table('chat_support')
          ->leftJoin('users', 'users.id', '=', 'chat_support.user_id')
          ->select('chat_support.*', 'users.name')
          ->where('chat_support.ans_id','0')
          ->orderBy('chat_support.id','DESC');
        if(isset($_GET['limit']) && !empty($_GET['limit'])){
          $paginationlimit = $_GET['limit'];
        }
        if(isset($_GET['name']) && !empty($_GET['name'])){
          $name = $_GET['name'];
          // $pages = $pages->orwhere('chat_support.title', 'like', "%$name%")
          // ->orwhere('users.name', 'like', "%$name%");
          $pages = $pages->where('users.name', 'like', "%$name%");
        } 
        if(isset($_GET['daterange']) && !empty($_GET['daterange'])){
          $daterange = $_GET['daterange'];
          //echo $daterange; die;
          $daterange = explode(" - ", $daterange);
          $startdate = $daterange[0];
          $enddate = $daterange[1];
          $today = date("Y-m-d");
          if($startdate != $today){
            // echo $startdate."==".$enddate; die;
            $pages = $pages->WhereBetween('chat_support.created_at',[$startdate, $enddate]);
          }
        }
        // $pages =  $pages->where('chat_support.title','');
        // $pages =  $pages->orderBy('chat_support.id','DESC');
  			$pages =  $pages->paginate($paginationlimit);
  			$active = "Chat";
  			$active_menu = "Chatsupport";
  			return view('page/chat_support',compact('pages','active','active_menu'));
		  }else{
       		return Redirect::to('/');
  		}
    }
    public function view_chat($id)
    {
    	$user = Auth::user();
    	$permission = $user->permission;
		  $permission = explode(",", $permission);
     	if($user->role == "1" || in_array("4", $permission)){
			$pages = DB::table('chat_support')
				->leftJoin('users', 'users.id', '=', 'chat_support.user_id')
				->leftJoin('admin', 'admin.id', '=', 'chat_support.admin_id')
				->select('chat_support.*', 'users.name', 'users.picture','admin.name as adminname','admin.picture as adminpicture')
				->where('ans_id',$id)
				->get();
			$Chatdata = DB::table('chat_support')
			->where('id',$id)
			->first();
			$active = "Chat";
			$active_menu = "Chatsupport";
			return view('page/view_chat',compact('Chatdata','pages','active','active_menu'));
		  }else{
       		return Redirect::to('/');
  		}
    }
    public function save_chat(Request $request)
    {
      //echo "<pre>"; print_r($_POST); die;
      $user = Auth::user();
      $user_id = $user->id;
      $ans_id  = $_POST['ans_id'];
      $mess  = $_POST['mess'];
      $created_at = date("Y-m-d h:i:sa"); 
      $created=DB::table('chat_support')
          ->insert(array(
          'admin_id'      => $user_id,
          'mess'      => $mess,
          'ans_id' => $ans_id,
          'created_at' => $created_at
      ));
      $request->session()->flash('success', 'Your message has been send');
      return redirect()->back();
    }
    public function remove_chat(Request $request)
    {
      $teams = $_POST['clients'];
      $teams1=DB::table('chat_support')->whereIn('id', $teams)->delete();
      $teams2=DB::table('chat_support')->whereIn('ans_id', $teams)->delete();
      //echo "<pre>"; print_r($_POST); die;
      $request->session()->flash('success', 'Your message has been deleted');
      //return Redirect::to('/chat-support');
      return redirect()->back();
    }
    // This function use for manage-chat END START --
    // This function use for manage-Sub-admin START --
    public function sub_admin()
    {
    	$user = Auth::user();
     	if($user->role == "1"){
        $paginationlimit = '1';
        $pages = DB::table('admin')
                ->orderBy('id','DESC')
                ->where('role','2');
        if(isset($_GET['limit']) && !empty($_GET['limit'])){
          $paginationlimit = $_GET['limit'];
        }
        if(isset($_GET['name']) && !empty($_GET['name'])){
          $name = $_GET['name'];
          $pages = $pages->where('name', 'like', "%$name%");
        } 
        if(isset($_GET['daterange']) && !empty($_GET['daterange'])){
          $daterange = $_GET['daterange'];
          //echo $daterange; die;
          $daterange = explode(" - ", $daterange);
          $startdate = $daterange[0];
          $enddate = $daterange[1];
          $today = date("Y-m-d");
          if($startdate != $today){
            // echo $startdate."==".$enddate; die;
            $pages = $pages->WhereBetween('created_at',[$startdate, $enddate]);
          }
        }
        if(isset($_GET['status']) && !empty($_GET['status'])){
          $status = $_GET['status'];
          if($status != "Select status"){
            if($status == 2){
              $status = 0;
            }else{
              $status = 1;
            }
            // echo $startdate."==".$enddate; die;
            $pages = $pages->Where('status',$status);
          }
        }
        $pages = $pages->paginate($paginationlimit);
  			// $pages = DB::table('admin')->orderBy('id','DESC')
  			// ->where('role','2')
  			// ->get();
  			// echo "<pr>"; print_r($pages); die;
  			$active = "Subadmin";
  			$active_menu = "Subadmins"; 
  			return view('page/sub_admin',compact('pages','active','active_menu'));
		  }else{
       		return Redirect::to('/');
  		}
    }
    public function remove_admin(Request $request)
    {

      $teams = $_POST['clients'];
      $teams=DB::table('admin')->whereIn('id', $teams)->delete();
      //echo "<pre>"; print_r($_POST); die;
      $request->session()->flash('success', 'Your sub-admin has been deleted');
      //return Redirect::to('/sub-admin');
      return redirect()->back();
    }
    public function update_admin()
    {
      //echo "<pre>"; print_r($_GET); die;
      $id    = $_POST['id'];
      $status = $_POST['type'];
       if($status == 1){
          $status1 = "0";
        }else{
          $status1 = "1";
        }
        $update=DB::table('admin')
            ->where('id', $id)
            ->update(array(
            'status' => $status1
        ));
        if($status == 1){
          return "0";
        }else{
          return "1";
        }
    }
    public function add_admin()
    {
		  $user = Auth::user();
     	if($user->role == "1"){
			$active = "Subadmin";
			$active_menu = "Subadmins"; 
			return view('page/add_admin',compact('active','active_menu'));
		  }else{
       		return Redirect::to('/');
  		}
    }
    public function save_admin(Request $request)
    {
      //echo "<pre>"; print_r($_POST); print_r($_FILES); die;
      $name1  = $_POST['name'];
      $email  = $_POST['email'];
      $pages = DB::table('admin')
        ->where('email',$email)
        ->first();
      //echo "<pre>"; print_r($pages); die;
      if(isset($pages) && !empty($pages)) {  
          $request->session()->flash('emailerror', 'Email address already exist');
          return redirect()->back();
      }else{
        $password  = $_POST['password'];
        $password  = Hash::make($password);
        $today = date("Y-m-d h:i:sa"); 
        if(!empty($_FILES['picture']['name'])){
            $image = $_FILES['picture']['name'];
            $temp_anme = $_FILES["picture"]["tmp_name"];
            $name = rand(10,99999).$image;
            $uploadfile = $_SERVER['DOCUMENT_ROOT'] . '/jatt_juliet/public/users/';
            $imagePath = $uploadfile. $name;
            move_uploaded_file($temp_anme,$imagePath);
            $hm_image = $name;
        }else{
            $hm_image = "";
        }
        $update=DB::table('admin')
            ->insert(array(
            'name'      => $name1,
            'email'      => $email,
            'password' => $password,
            'picture' => $hm_image,
            'role' => '2',
            'created_at' => $today
        ));
        $request->session()->flash('success', 'Your sub-admin has been added');
        return Redirect::to('/sub-admin');
      }
    }
    public function edit_admin($id)
    {
    	$user = Auth::user();
     	if($user->role == "1"){
			$pages = DB::table('admin')
			->where('id',$id)
			->first();
			$active = "Subadmin";
			$active_menu = "Subadmins"; 
			return view('page/edit_admin',compact('pages','active','active_menu'));
		  }else{
       		return Redirect::to('/');
  		}
    }
    public function update_subadmin(Request $request)
    {
      //echo "<pre>"; print_r($_POST); print_r($_FILES); die;
      $id  = $_POST['id'];
      $name1  = $_POST['name'];
      //$email  = $_POST['email'];
      if(isset($_POST['password']) && !empty($_POST['password'])){
        $password  = $_POST['password'];
        $password  = Hash::make($password);
      }else{
        $password  = $_POST['old_pass'];
      }
      $today = date("Y-m-d h:i:sa"); 
      $old_image  = $_POST['old_image'];
      if(!empty($_FILES['picture']['name'])){
          $image = $_FILES['picture']['name'];
          $temp_anme = $_FILES["picture"]["tmp_name"];
          $name = rand(10,99999).$image;
          $uploadfile = $_SERVER['DOCUMENT_ROOT'] . '/jatt_juliet/public/users/';
          $imagePath = $uploadfile. $name;
          move_uploaded_file($temp_anme,$imagePath);
          @unlink($uploadfile.$old_image);
          $hm_image = $name;
      }else{
          $hm_image = $old_image;
      }
      $update=DB::table('admin')
          ->where('id', $id)
          ->update(array(
          'name'      => $name1,
          //'email'      => $email,
          'password' => $password,
          'picture' => $hm_image,
          'created_at' => $today
      ));
      $request->session()->flash('success', 'Your sub-admin has been updated');
      return Redirect::to('/sub-admin');
    }
    public function permission($id)
    {
    	$user = Auth::user();
     	if($user->role == "1"){
			$pages = DB::table('admin')
			->where('id',$id)
			->first();
			$active = "Subadmin";
			$active_menu = "Subadmins"; 
			return view('page/permission',compact('pages','active','active_menu'));
		  }else{
       		return Redirect::to('/');
  		}
    }
    public function save_permission(Request $request)
    {
      //echo "<pre>"; print_r($_POST); // die;
      $id  = $_POST['id'];
      $permission  = $_POST['permission'];
      $permission = implode(",", $permission);
      $update=DB::table('admin')
          ->where('id', $id)
          ->update(array(
          'permission'  => $permission
      ));
      $request->session()->flash('success', 'Sub-admin permission has been updated');
      return redirect()->back();
    }
    // This function use for manage-Sub-admin END --
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('page/create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(PageFormRequest $request)
    {
        $page = Page::create($request->all());

        alert()->success('Page has been added.');

        return redirect()->route('page.edit', $page->id);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Page $page)
    {
        return view('page/show', compact('page'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Page $page)
    {
        return view('page/edit', compact('page'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(PageFormRequest $request, Page $page)
    {
        $page->update($request->all());

        alert()->success('Page has been updated.');

        return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Page::destroy($id);

        alert()->success('Page has been deleted.');

        return redirect('/page');
    }
}
