<?php
Auth::routes();
Route::get('/', 'PageController@index');
Route::get('page/add', 'PageController@create');
Route::get('page/{page}/delete', [
    'as'   => 'page.delete',
    'uses' => 'PageController@destroy',
]);


//---/manage-users--//
Route::get('/manage-users', 'PageController@users');
Route::get('/view-users/{id}', 'PageController@view_users');
Route::post('/remove-users', 'PageController@remove_users');
Route::post('/update-users', 'PageController@update_users');
//---Manage-video--//
Route::post('/remove-video', 'PageController@remove_video');
Route::get('/users-video', 'PageController@manage_video');
Route::get('/view-video/{id}', 'PageController@view_video');
Route::get('/delete-video/{id}', 'PageController@delete_video');
//---category--//
Route::get('/manage-category', 'PageController@manage_category');
Route::get('/add-category', 'PageController@add_category');
Route::post('/save-category', 'PageController@save_category');
Route::get('/edit-category/{id}', 'PageController@edit_category');
Route::post('/update-category', 'PageController@update_category');
Route::post('/remove-category', 'PageController@remove_category');
//---sound--//
Route::get('/manage-sound', 'PageController@manage_sound');
Route::get('/add-sound', 'PageController@add_sound');
Route::post('/save-sound', 'PageController@save_sound');
Route::get('/edit-sound/{id}', 'PageController@edit_sound');
Route::post('/update-sound', 'PageController@update_sound');
Route::post('/remove-sound', 'PageController@remove_sound');
//---chat-support--//
Route::get('/chat-support', 'PageController@chat_support');
Route::get('/view-chat/{id}', 'PageController@view_chat');
Route::post('/save-chat/', 'PageController@save_chat');
Route::post('/remove-chat/', 'PageController@remove_chat');
//---Sub-admin--//
Route::get('/sub-admin', 'PageController@sub_admin');
Route::post('/remove-admin', 'PageController@remove_admin');
Route::post('/update-admin', 'PageController@update_admin');
Route::get('/add-admin', 'PageController@add_admin');
Route::post('/save-admin', 'PageController@save_admin');
Route::get('/edit-admin/{id}', 'PageController@edit_admin');
Route::post('/update-subadmin', 'PageController@update_subadmin');
Route::get('/permission/{id}', 'PageController@permission');
Route::post('/save-permission', 'PageController@save_permission');

//API ----//
Route::post('/api/register', 'WebserviceController@register');
Route::get('/api/profile', 'WebserviceController@profile');
Route::get('/api/profile/{id}', 'WebserviceController@profile');
Route::post('/api/update-profile', 'WebserviceController@update_profile');
Route::post('/api/chack-slug', 'WebserviceController@chack_slug');
Route::get('/api/video', 'WebserviceController@video');


function is_active_sorter($key, $direction = 'ASC')
{
    if (request('sortby') == $key && request('sortdir') == $direction) {
        return true;
    }

    return false;
}


