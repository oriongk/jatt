-- phpMyAdmin SQL Dump
-- version 4.8.2
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Aug 22, 2018 at 11:21 AM
-- Server version: 10.1.34-MariaDB
-- PHP Version: 7.2.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `jatt_juliet`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE `admin` (
  `id` bigint(20) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `picture` varchar(255) DEFAULT NULL,
  `role` int(1) NOT NULL DEFAULT '2',
  `status` int(1) NOT NULL DEFAULT '1',
  `remember_token` varchar(255) DEFAULT NULL,
  `permission` varchar(255) DEFAULT NULL,
  `created_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`id`, `name`, `email`, `password`, `picture`, `role`, `status`, `remember_token`, `permission`, `created_at`) VALUES
(1, 'admin', 'admin@admin.com', '$2y$10$f5rGmZ/TL9zLyXDmkVMlJ.Hi9ivBr7MEZdvG0yYjKFnNk3/a32X1K', '1495188244-88226.jpg', 1, 1, 'APvIYnYJfD81q7lP4WIQDpRYrYpM0cQlaDFk5ehabfifjqoPPe0fQsL50Ipo', NULL, '0000-00-00 00:00:00'),
(2, 'kp', 'kp@gmail.com', '$2y$10$jqMKJuwI/dEooZkID00lLOQpAkjK5bCvHTpG.3gvRiimUoHJT1RYW', '1785714114006_1068334999940137_1979487850_o.jpg', 2, 1, '3H8kIBzRYC4jAPIlVRsOAIpPWwLUZr0oRIXCFTDGfRn2zvqDRaIlHA0hQcUt', '4,2', '2018-08-21 09:00:30'),
(4, 'kpsingh', 'kpsingh@gmail.com', '$2y$10$XEX51JxPyK7mQDwaSthH6.FloqaQRJvEaORm6.Jy/iEBDaGc4AdSa', '92945download.jpeg', 2, 0, 'izqhBiWgFNNg50hlUzFy03E7rOzlZy3iPp9kYvRbI3iSXUhHiUwVKpHWpF5D', '1,2,3', '2018-08-21 09:45:53');

-- --------------------------------------------------------

--
-- Table structure for table `category`
--

CREATE TABLE `category` (
  `category_id` bigint(20) NOT NULL,
  `category_title` varchar(255) NOT NULL,
  `category_image` varchar(255) NOT NULL,
  `category_home_status` enum('0','1') NOT NULL DEFAULT '1',
  `category_added_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `category`
--

INSERT INTO `category` (`category_id`, `category_title`, `category_image`, `category_home_status`, `category_added_date`) VALUES
(12, 'Transitional', '', '1', '2017-09-28 02:03:52'),
(13, 'Commercial', '342661_Lo8cD5m7VqUXPlKF3lgfNQ.jpeg', '1', '2018-08-18 12:33:27'),
(14, 'test', '74145stock-photo-indian-flag-on-a-motorcycle-151578308.jpg', '1', '2018-08-18 12:33:41');

-- --------------------------------------------------------

--
-- Table structure for table `chat_support`
--

CREATE TABLE `chat_support` (
  `id` bigint(20) NOT NULL,
  `user_id` bigint(20) DEFAULT NULL,
  `admin_id` bigint(20) NOT NULL DEFAULT '0',
  `title` varchar(255) DEFAULT NULL,
  `mess` text,
  `status` int(1) NOT NULL DEFAULT '0',
  `ans_id` bigint(20) NOT NULL DEFAULT '0',
  `created_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `chat_support`
--

INSERT INTO `chat_support` (`id`, `user_id`, `admin_id`, `title`, `mess`, `status`, `ans_id`, `created_at`) VALUES
(1, 1, 0, 'test', '', 0, 0, '2018-08-20 00:00:00'),
(2, 1, 0, 'test 2', '', 0, 0, '2018-08-20 00:00:00'),
(3, 1, 0, NULL, 'hello admin', 0, 1, '2018-08-20 00:00:00'),
(4, NULL, 1, NULL, 'hi user ', 0, 1, '2018-08-21 02:18:12'),
(5, 1, 0, NULL, 'you can help me', 0, 1, '2018-08-21 00:00:00'),
(6, NULL, 1, NULL, 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s', 0, 1, '2018-08-21 06:34:19'),
(7, 1, 0, NULL, 'test dugf d', 0, 1, '2018-08-21 06:38:32'),
(8, 1, 0, NULL, 'hello', 0, 1, '2018-08-21 06:48:32'),
(9, NULL, 4, NULL, 'hi', 0, 1, '2018-08-21 12:08:29');

-- --------------------------------------------------------

--
-- Table structure for table `sound`
--

CREATE TABLE `sound` (
  `id` bigint(20) NOT NULL,
  `category_id` bigint(20) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `sound` varchar(255) DEFAULT NULL,
  `image` varchar(255) DEFAULT NULL,
  `status` int(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sound`
--

INSERT INTO `sound` (`id`, `category_id`, `title`, `sound`, `image`, `status`) VALUES
(1, 12, 'Test', 'SampleAudio_0.4mb.mp3', '342661_Lo8cD5m7VqUXPlKF3lgfNQ.jpeg', 1),
(2, 13, 'Test 2', 'SampleAudio_0.7mb.mp3', NULL, 1),
(3, 14, 'test 3', '43789SampleAudio_0.4mb.mp3', '', 1);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `gender` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `phone` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `picture` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `social_type` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `status` int(1) NOT NULL DEFAULT '1',
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `gender`, `phone`, `picture`, `social_type`, `status`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Kp', 'kp@demo.com', '$2y$10$f5rGmZ/TL9zLyXDmkVMlJ.Hi9ivBr7MEZdvG0yYjKFnNk3/a32X1K', 'M', '919672781314', '15163662881516366288-56258.jpg', 'Normal', 1, 'FKCl6lqwnLjB9OaN8JxDGGYBzEiOJZrtF81KiSoAwicc1LaI45LBYVot7Oip', '2018-07-17 06:58:17', '2018-07-17 06:58:17');

-- --------------------------------------------------------

--
-- Table structure for table `video`
--

CREATE TABLE `video` (
  `id` bigint(20) NOT NULL,
  `user_id` bigint(20) DEFAULT NULL,
  `video` varchar(255) DEFAULT NULL,
  `status` int(1) NOT NULL DEFAULT '1',
  `created_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `video`
--

INSERT INTO `video` (`id`, `user_id`, `video`, `status`, `created_at`) VALUES
(1, 1, '11122.mp4', 1, '2018-08-17 00:00:00'),
(2, 1, '11122.mp4', 1, '2018-08-17 00:00:00'),
(3, 1, '11122.mp4', 1, '0000-00-00 00:00:00');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `category`
--
ALTER TABLE `category`
  ADD PRIMARY KEY (`category_id`);

--
-- Indexes for table `chat_support`
--
ALTER TABLE `chat_support`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sound`
--
ALTER TABLE `sound`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- Indexes for table `video`
--
ALTER TABLE `video`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin`
--
ALTER TABLE `admin`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `category`
--
ALTER TABLE `category`
  MODIFY `category_id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `chat_support`
--
ALTER TABLE `chat_support`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `sound`
--
ALTER TABLE `sound`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `video`
--
ALTER TABLE `video`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
