  <?php use App\Page; 

  ?>
  @if (session()->has('success'))
<div class="jq-toast-wrap top-right">
  <div class="jq-toast-single jq-has-icon jq-icon-success" style="text-align: left; display: block;">
    <span class="jq-toast-loader"></span>
    <h2 class="jq-toast-heading">{!! session()->get('success') !!}</h2>
  </div>
</div>
@endif
  <nav class="navbar navbar-default navbar-static-top m-b-0">
    <div class="navbar-header"> <a class="navbar-toggle hidden-sm hidden-md hidden-lg " href="javascript:void(0)" data-toggle="collapse" data-target=".navbar-collapse"><i class="ti-menu"></i></a>
      <div class="top-left-part"><a class="logo" href="<?php echo url('/'); ?>"><b><img src="<?php echo url('/'); ?>/assets/pics/adminLogo.png" alt="home" style="max-width:30px;" /></b><span class="hidden-xs">Jatt Juliet</span></a></div>
      <ul class="nav navbar-top-links navbar-left hidden-xs">
        <li><a href="javascript:void(0)" class="open-close hidden-xs waves-effect waves-light"><i class="icon-arrow-left-circle ti-menu"></i></a></li>
        
      </ul>
       <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                    {{ csrf_field() }}
                </form>
      <ul class="nav navbar-top-links navbar-right pull-right">
        
        <li class="waves-effect waves-light"> <a href="{{ route('logout') }}"
                   onclick="event.preventDefault();
                            document.getElementById('logout-form').submit();" ><i class="fa fa-power-off"></i> Logout</a></li>
      </ul>
    </div>
</nav>  
    <div class="navbar-default sidebar" role="navigation">
    <div class="sidebar-nav navbar-collapse slimscrollsidebar">
       <ul class="nav" id="side-menu">
           <li class="sidebar-search hidden-sm hidden-md hidden-lg">
          <div class="input-group custom-search-form">
            <input type="text" class="form-control" placeholder="Search...">
            <span class="input-group-btn">
            <button class="btn btn-default" type="button"> <i class="fa fa-search"></i> </button>
            </span> </div>
        </li>
        <li class="user-pro <?php if($active_menu == "ADMIN") { echo "active"; } ?>">
          <?php $checkedp =  Page::admin_details(); ?>
          <a href="<?php echo url('/'); ?>" class="waves-effect" id="profile-avatar-link">
            <?php if(!empty($checkedp->picture)) { ?>
              <img src="<?php echo url('/'); ?>/users/<?php echo $checkedp->picture ?>" class="img-circle" alt="img" />
            <?php
            }else{ ?>
                <img src="<?php echo url('/'); ?>/users/default.png" class="img-circle" alt="img" />
            <?php
            } ?>
            <span class="hide-menu"> ADMIN <span class="fa arrow"></span></span>
            </a>
          <ul class="nav nav-second-level collapse">
           <!--  <li class="<?php if($active_menu == "ADMIN") { echo "active"; } ?>"><a href="<?php echo url('/'); ?>/profile"><i class="ti-user"></i> My Profile</a></li> -->
            <li><a href="{{ route('logout') }}"
                   onclick="event.preventDefault();
                            document.getElementById('logout-form').submit();" ><i class="fa fa-power-off"></i> Logout</a></li>
          </ul>
        </li>
<li class='<?php if($active_menu == "Dashboard") { echo "active"; } ?>'>
<a class="waves-effect" href="<?php echo url('/'); ?>">
<i class="zmdi zmdi-view-dashboard zmdi-hc-fw fa-fw"></i>
<span class="hide-menu">Dashboard</span>
</a>
</li>
<?php $permission = $checkedp->permission;
$permission = explode(",", $permission);
if($checkedp->role == "1" || in_array("1", $permission)){  ?>
<li class='has-sub <?php if($active_menu == "ManageUsers") { echo "active"; } ?>'>
  <a class="waves-effect   has-sub " href="javascript:void(1);">
  <i class="zmdi zmdi-accounts zmdi-hc-fw fa-fw"></i>
  <span class="hide-menu">Manage Users</span>
  <span class="fa arrow"></span>
  <span class="fa arrow"></span>
  </a>
  <ul class='nav nav-second-level collapse'>
  <li class="<?php if($active == "Users") { echo "active"; } ?>">
  <a class="" href="<?php echo url('/'); ?>/manage-users">
  Manage Users</a>
  </li>
  </ul>
</li>
<?php } 
if($checkedp->role == "1" || in_array("2", $permission)){  ?>
<li class=' has-sub <?php if($active_menu == "AdminVideo") { echo "active"; } ?>'>
  <a class="waves-effect   has-sub " href="javascript:void(1);">
  <i class="zmdi zmdi-movie"></i>
  <span class="hide-menu">Manage Video</span>
  <span class="fa arrow"></span>
  <span class="fa arrow"></span>
  </a>
  <ul class='nav nav-second-level collapse'>
  <li class="<?php if($active == "Video") { echo "active"; } ?>">
  <a class="" href="<?php echo url('/'); ?>/users-video">
  Video</a>
  </li>
  </ul>
</li>
<?php } 
if($checkedp->role == "1" || in_array("3", $permission)){  ?>
<li class=' has-sub <?php if($active_menu == "SoundC") { echo "active"; } ?>'>
  <a class="waves-effect   has-sub " href="javascript:void(1);">
  <i class="zmdi zmdi-playlist-audio"></i>
  <span class="hide-menu">Sound Categories</span>
  <span class="fa arrow"></span>
  <span class="fa arrow"></span>
  </a>
  <ul class='nav nav-second-level collapse'>
  <li class="<?php if($active == "Categories") { echo "active"; } ?>">
  <a class="" href="<?php echo url('/'); ?>/manage-category">
  Categories</a>
  </li>
  <li class="<?php if($active == "Sound") { echo "active"; } ?>">
  <a class="" href="<?php echo url('/'); ?>/manage-sound">
  Sound</a>
  </li>
  </ul>
</li>
<?php } 
if($checkedp->role == "1" || in_array("4", $permission)){  ?>
<li class=' has-sub <?php if($active_menu == "Chatsupport") { echo "active"; } ?>'>
  <a class="waves-effect   has-sub " href="javascript:void(1);">
  <i class="zmdi zmdi-keyboard"></i>
  <span class="hide-menu">Chat support</span>
  <span class="fa arrow"></span>
  <span class="fa arrow"></span>
  </a>
  <ul class='nav nav-second-level collapse'>
  <li class="<?php if($active == "Chat") { echo "active"; } ?>">
  <a class="" href="<?php echo url('/'); ?>/chat-support">
  Chat</a>
  </li>
  </ul>
</li>
<?php } 
if($checkedp->role == "1" ){  ?>
<li class=' has-sub <?php if($active_menu == "Subadmins") { echo "active"; } ?>'>
  <a class="waves-effect   has-sub " href="javascript:void(1);">
  <i class="zmdi zmdi-accounts zmdi-hc-fw fa-fw"></i>
  <span class="hide-menu">Manage sub-admin</span>
  <span class="fa arrow"></span>
  <span class="fa arrow"></span>
  </a>
  <ul class='nav nav-second-level collapse'>
  <li class="<?php if($active == "Subadmin") { echo "active"; } ?>">
  <a class="" href="<?php echo url('/'); ?>/sub-admin">
  Sub-admin</a>
  </li>
  </ul>
</li>
<?php } ?>
</ul>
    </div>
</div>

