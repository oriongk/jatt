<!DOCTYPE html>
<!--[if lt IE 7]> <html class="lt-ie9 lt-ie8 lt-ie7" lang="en"> <![endif]-->
<!--[if IE 7]>    <html class="lt-ie9 lt-ie8" lang="en"> <![endif]-->
<!--[if IE 8]>    <html class="lt-ie9" lang="en"> <![endif]-->
<!--[if gt IE 8]><!--><html lang="en"><!--<![endif]-->
<head>
<meta charset="utf-8" />
<link rel="icon" type="image/png" sizes="16x16" href="assets/pics/fav.png">
<meta http-equiv="Content-Type" content="text/html;charset=utf-8" >
<meta name="keywords" content="Wedding" >
<meta name="description" content="Wedding,directid" >
<title>Jatt Juliet | <?php echo ucfirst($Chatdata->title); ?></title>
<link href="<?php echo url('/'); ?>/assets/bootstrap/css/bootstrap.min.css" media="screen" rel="stylesheet" type="text/css" >
<link href="<?php echo url('/'); ?>/assets/sidebar-nav/dist/sidebar-nav.min.css" media="screen" rel="stylesheet" type="text/css" >
<link href="<?php echo url('/'); ?>/assets/morrisjs/morris.css" media="screen" rel="stylesheet" type="text/css" >
<link href="<?php echo url('/'); ?>/assets/css/animate.css" media="screen" rel="stylesheet" type="text/css" >
<link href="<?php echo url('/'); ?>/assets/css/style.css" media="screen" rel="stylesheet" type="text/css" >
<link href="<?php echo url('/'); ?>/assets/css/colors/default.css" media="screen" rel="stylesheet" type="text/css" >
<link href="<?php echo url('/'); ?>/assets/css/colors/blue.css" media="screen" rel="stylesheet" type="text/css" >
<link href="<?php echo url('/'); ?>/assets/toast/css/jquery.toast.css" media="screen" rel="stylesheet" type="text/css" >
<link href="<?php echo url('/'); ?>/assets/sweetalert/sweetalert.css" media="screen" rel="stylesheet" type="text/css" >
<link href="<?php echo url('/'); ?>/assets/html5-editor/bootstrap-wysihtml5.css" media="screen" rel="stylesheet" type="text/css" ><!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
  <script src="/assets/js/html5shiv.js"></script>
  <script src="/assets/js/respond.min.js"></script>
<![endif]-->
<script src="<?php echo url('/'); ?>/assets/js/web.min.js"></script>
<script type="text/javascript" src="<?php echo url('/'); ?>/assets/js/webmig.min.js"></script>
<script type="text/javascript">
  var LoggedUser=1;
  var ADMIN_APPURL="<?php echo url('/'); ?>";
  var AVTURL="<?php echo url('/'); ?>";
  var MEDIAURL="<?php echo url('/'); ?>";
  var AVTBIGURL="<?php echo url('/'); ?>";
  var SITENAME="Jatt Juliet";
  var Action="editfrontpage";
  var Controller="static";
  var ConfirmTitle="Are you sure?";
  var ConfirmBtn="Yes";
  var CancelBtn="Cancel";
  var extError="Uploaded file is not a valid image. Only JPG,PNG and JPEG files are allowed.";
  var AvtUpdated="Your profile avatar has been updated";
  var PwdUserError="Username and password must not be same";
</script>
<style type="text/css">
.chat
{
    list-style: none;
    margin: 0;
    padding: 0;
}

.chat li
{
    margin-bottom: 10px;
    padding-bottom: 5px;
    border-bottom: 1px dotted #B3A9A9;
}

.chat li.left .chat-body
{
    margin-left: 60px;
}

.chat li.right .chat-body
{
    margin-right: 60px;
}


.chat li .chat-body p
{
    margin: 0;
    color: #777777;
}

.panel .slidedown .glyphicon, .chat .glyphicon
{
    margin-right: 5px;
}

.panel-body
{
    overflow-y: scroll;
    height: 300px;
}

::-webkit-scrollbar-track
{
    -webkit-box-shadow: inset 0 0 6px rgba(0,0,0,0.3);
    background-color: #F5F5F5;}
#mess-error{
  color: #f24444 !important;
}
</style>
</head>
<body class="fix-sidebar fix-header">
<div class="preloader"><svg class="circular" viewBox="25 25 50 50"><circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10"/></svg></div>
<div id="wrapper">  
  
    @extends('layouts.left-side')
<!--<li class="nav-small-cap m-t-10">--- Main Menu</li>-->     <div id="page-wrapper">
      <div class="container-fluid">
          <div class="row bg-title">
    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
      <h4 class="page-title"><?php echo ucfirst($Chatdata->title); ?></h4>
    </div>
    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
      &nbsp;
    </div>
</div>            <div class="col-md-12">
    <ul class="page-breadcrumb breadcrumb">
      <a href="<?php echo url('/'); ?>" style="color:#FFF;"><i class="fa fa-dashboard"></i> Dashboard</a>  
      <i class="fa fa-chevron-right" style="vertical-align:middle; color:#FFF;"></i>
      <a href="javascript:void(1);" style="color:#FFF;">Chat support</a>  
      <i class="fa fa-chevron-right" style="vertical-align:middle; color:#FFF;"></i>
      <a href="<?php echo url('/'); ?>/chat-support" style="color:#FFF;">Chat</a>  
      <i class="fa fa-chevron-right" style="vertical-align:middle; color:#FFF;"></i>
      <a href="" style="color:#FFF;"><?php echo ucfirst($Chatdata->title); ?></a>  
    </ul>
</div>
<div style="clear:both;"></div>
      <div class="row">
  <div class="white-box">
  <!-- //-Chat -->
  <div class="panel-collapse collapse in" id="collapseOne" style="">
  <div class="panel-body">
  <ul class="chat">
    <?php
    if(isset($pages[0]) && !empty($pages[0])){
      foreach ($pages as $key => $value) 
      { 
        if($value->admin_id != "0") { 
        ?>
          <li class="left clearfix">
            <span class="chat-img pull-left">
              <img src="<?php echo url('/'); ?>/users/<?php echo $value->adminpicture ?>" alt="User Avatar" class="img-circle" style="width: 50px; height: 50px">
            </span>
            <div class="chat-body clearfix">
              <div class="header">
                <strong class="primary-font"><?php echo $value->adminname; ?></strong> <small class="pull-right text-muted">
                <span class="glyphicon glyphicon-time"></span><?php echo date('F d,Y h:i a', strtotime($value->created_at)); ?></small>
              </div>
              <p><?php echo $value->mess; ?></p>
            </div>
          </li>
          <?php
        }else{ ?>
          <li class="right clearfix">
            <span class="chat-img pull-right">
              <img src="<?php echo url('/'); ?>/users/<?php echo $value->picture ?>" alt="User Avatar" class="img-circle" style="width: 50px; height: 50px">
            </span>
            <div class="chat-body clearfix">
              <div class="header">
                <small class=" text-muted"><span class="glyphicon glyphicon-time"></span><?php echo date('F d,Y h:i a', strtotime($value->created_at)); ?></small>
                <strong class="pull-right primary-font"><?php echo $value->name; ?></strong>
              </div>
              <p><?php echo $value->mess; ?></p>
            </div>
          </li>
        <?php
        }
      }
    }else{ ?>
      <li class="right clearfix">
        No message found !
      </li>
      <?php
    } ?>
  </ul>
  </div>
    <div class="panel-footer">
      <form id="page-form" enctype="multipart/form-data" role="form" class="" action="<?php echo url('/'); ?>/save-chat" novalidate method="post">
      <input type="hidden" name="_token" value="{{ csrf_token() }}">
      <input type="hidden" name="ans_id" value="<?php echo ucfirst($Chatdata->id); ?>">
      <div class="input-group">
        <input type="text" name="mess" id="mess" value="" class="form-control required" placeholder="Type your message here..." autocomplete="off" required="" aria-required="true">
        <span class="input-group-btn">
        <button name="bttnsubmit" id="bttnsubmit" type="submit" value="
        Save" class="btn fcbtn btn-outline btn-info btn-1e btn btn-default">
        Send</button>
        </span>
      </div>
      </form>
    </div>
  </div>
  <!-- //-Chat -->
  </div>
</div>
<script type="text/javascript">
  var height = 0;
  $('.panel-body li').each(function(i, value){
  height += parseInt($(this).height());
  });
  height += '';
  $('.panel-body').animate({scrollTop: height});
  $(document).ready(function(e) {
      $('#page-form').validate({
      ignore:":hidden:not(textarea)",     
    });
      $('.editor').wysihtml5();
    if($('a[data-wysihtml5-command="insertImage"]').length>0){
      $('a[data-wysihtml5-command="insertImage"]').remove();
    }
    });
</script>   </div>
    <footer class="footer text-center"><?php echo date("Y"); ?> &copy; Jatt Juliet </footer>
<script type="text/javascript" defer="defer" src="<?php echo url('/') ?>/js/bootsetrap.min.js"></script>
<script type="text/javascript" defer="defer" src="<?php echo url('/'); ?>/assets/js/vanilla.idle.js"></script>
<script type="text/javascript" defer="defer" src="<?php echo url('/'); ?>/assets/bootstrap/js/bootstrap.min.js"></script>
<script type="text/javascript" defer="defer" src="<?php echo url('/'); ?>/assets/js/jquery.slimscroll.js"></script>
<script type="text/javascript" defer="defer" src="<?php echo url('/'); ?>/assets/js/waves.js"></script>
<script type="text/javascript" defer="defer" src="<?php echo url('/'); ?>/assets/sidebar-nav/dist/sidebar-nav.min.js"></script>
<script type="text/javascript" defer="defer" src="<?php echo url('/'); ?>/assets/jquery-validation/jquery.validate.min.js"></script>
<script type="text/javascript" defer="defer" src="<?php echo url('/'); ?>/assets/jquery-validation/additional-methods.min.js"></script>
<script type="text/javascript" defer="defer" src="<?php echo url('/'); ?>/assets/sweetalert/sweetalert.min.js"></script>
<script type="text/javascript" defer="defer" src="<?php echo url('/'); ?>/assets/toast/js/jquery.toast.js"></script>
<script type="text/javascript" defer="defer" src="<?php echo url('/'); ?>/assets/js/initial.min.js"></script>
<script type="text/javascript" defer="defer" src="<?php echo url('/'); ?>/assets/js/js.cookie.js"></script>
<script type="text/javascript" defer="defer" src="<?php echo url('/'); ?>/assets/js/mask.js"></script>
<script type="text/javascript" defer="defer" src="<?php echo url('/'); ?>/assets/ckeditor/ckeditor.js"></script>
<script type="text/javascript" defer="defer" src="<?php echo url('/'); ?>/assets/js/custom.min.js"></script>
<script type="text/javascript" defer="defer" src="<?php echo url('/'); ?>/assets/html5-editor/wysihtml5-0.3.0.js"></script>
<script type="text/javascript" defer="defer" src="<?php echo url('/'); ?>/assets/html5-editor/bootstrap-wysihtml5.js"></script>
<script type="text/javascript" defer="defer" src="<?php echo url('/'); ?>/assets/js/jquery-ui.min.js"></script>
<script type="text/javascript" defer="defer" src="<?php echo url('/'); ?>/assets/js/jasny-bootstrap.js"></script>
<script type="text/javascript" defer="defer" src="<?php echo url('/'); ?>/assets/jquery-file-upload/js/jquery.fileupload.js"></script>


    </div>
</div>
<div id="admin-webapp-modal" class="modal fade in" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        <h4 class="modal-title"></h4>
      </div>
      <div class="modal-body"></div>
    </div>
  </div>
</div>
</body>  
</html>